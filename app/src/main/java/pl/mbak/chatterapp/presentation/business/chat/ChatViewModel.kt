package pl.mbak.chatterapp.presentation.business.chat

import android.util.Log
import androidx.hilt.Assisted
import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.*
import pl.mbak.chatterapp.domain.usercases.SendMessageUseCase

/**
 *
 * Created by marcinbak on 28/10/2020.
 */
class ChatViewModel @ViewModelInject constructor(
    @Assisted private val savedStateHandle: SavedStateHandle,
    private val sendMessageUseCase: SendMessageUseCase,
    private val mapMessageDomainToViewUseCase: MapMessageDomainToViewUseCase,
) : ViewModel() {

    companion object {
        private const val TAG = "ChatViewModel"
        private const val MESSAGE_KEY = "MESSAGE_KEY"
    }

    val messageText: MutableLiveData<String> = savedStateHandle.getLiveData(MESSAGE_KEY)
    val sendButtonEnabled: LiveData<Boolean> = messageText.map { it.isNullOrEmpty().not() }

    private val _messagesList: LiveData<List<MessageVM>> = mapMessageDomainToViewUseCase.getMessages()
        .onErrorReturnItem(emptyList())
        .toLiveData()

    val messagesList: LiveData<List<MessageVM>> = _messagesList.map { it.reversed() }

    fun onSend() {
        val messageContent = messageText.value ?: return

        sendMessageUseCase.send(messageContent).subscribe()

        messageText.value = null
    }

    data class MessageVM(val fullName: String, val text: String, val date: String, val startOrEnd: Boolean)
}