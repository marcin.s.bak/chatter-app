package pl.mbak.chatterapp.presentation.business.chat

import dagger.Reusable
import io.reactivex.rxjava3.core.Flowable
import pl.mbak.chatterapp.data.repositories.AuthTokenRepository
import pl.mbak.chatterapp.domain.entity.ChatMessage
import pl.mbak.chatterapp.domain.usercases.GetMessagesUseCase
import java.time.LocalDate
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter
import javax.inject.Inject

@Reusable
class MapMessageDomainToViewUseCase @Inject constructor(
    private val getMessagesUseCase: GetMessagesUseCase,
    private val authTokenRepository: AuthTokenRepository,
) {

    fun getMessages(): Flowable<List<ChatViewModel.MessageVM>> {
        return getMessagesUseCase.get()
            .map { mapToMessageVM(it) }
    }

    private fun mapToMessageVM(list: List<ChatMessage>): List<ChatViewModel.MessageVM> {
        val userId = authTokenRepository.getUserId() ?: return emptyList()

        val now = LocalDate.now()
        val yesterday = now.minusDays(1)

        return list.map {
            ChatViewModel.MessageVM(
                fullName = "${it.author.firstName} ${it.author.lastName}",
                text = it.text,
                date = it.timeStamp.toFormatted(now, yesterday),
                startOrEnd = userId != it.author.id
            )
        }
    }

    private fun LocalDateTime.toFormatted(now: LocalDate, yesterday: LocalDate): String {
        val messageDate = this.toLocalDate()

        return when(messageDate) {
            now -> todayFormatter
            yesterday -> yesterdayFormatter
            else -> elseFormatter
        }.format(this)
    }

    private val todayFormatter = DateTimeFormatter.ofPattern("'Today at' HH:mm")
    private val yesterdayFormatter = DateTimeFormatter.ofPattern("'Yesterday at' HH:mm")
    private val elseFormatter = DateTimeFormatter.ofPattern("dd MMM yyyy 'at' HH:mm")
}
