package pl.mbak.chatterapp.presentation.ui.login

import android.net.Uri
import android.os.Bundle
import android.view.View
import androidx.activity.result.ActivityResultLauncher
import androidx.activity.result.contract.ActivityResultContracts
import androidx.core.content.FileProvider
import androidx.fragment.app.viewModels
import coil.load
import coil.transform.CircleCropTransformation
import dagger.hilt.android.AndroidEntryPoint
import pl.mbak.chatterapp.BuildConfig
import pl.mbak.chatterapp.R
import pl.mbak.chatterapp.presentation.base.DataBindingFragment
import pl.mbak.chatterapp.presentation.business.RegisterViewModel
import pl.mbak.chatterapp.databinding.FragmentRegisterBinding
import java.io.File

@AndroidEntryPoint
class RegisterFragment : DataBindingFragment<FragmentRegisterBinding>(R.layout.fragment_register) {

    companion object {
        private val TAG = "RegisterActivity"
        const val EMAIL_ARG_KEY = RegisterViewModel.EMAIL_KEY

        fun create(email: String? = null): RegisterFragment {
            val fragment = RegisterFragment()
            fragment.arguments = Bundle().apply {
                putString(EMAIL_ARG_KEY, email)
            }
            return fragment
        }
    }

    private val viewModel: RegisterViewModel by viewModels()

    private lateinit var takePhotoLauncher: ActivityResultLauncher<Uri>

    private lateinit var photosDir: File

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        takePhotoLauncher = registerForActivityResult(ActivityResultContracts.TakePicture()) {
            loadPhoto()
        }
        photosDir = File(requireContext().filesDir, "images")
        photosDir.mkdirs()
        loadPhoto()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        binding.apply {
            email = viewModel.email
            password = viewModel.password
            repeatPassword = viewModel.repeatPassword
            firstName = viewModel.firstName
            lastName = viewModel.lastName
            tosAccepted = viewModel.tosAccepted
            registerEnabled = viewModel.registerEnabled
            avatarVisible = viewModel.avatarVisible

            loadingVisible = viewModel.loadingVisible

            registerClicked = { viewModel.onCreateAccount() }
            cancelClicked = { requireActivity().onBackPressed() }
            photoClicked = { startCamera() }
        }

        viewModel.avatarImage.observe(viewLifecycleOwner) {}
    }

    private fun loadPhoto() {
        viewModel.avatarImage.value
            ?.let { File(photosDir, it) }
            ?.also { loadImageToAvatarView(it) }
    }

    private fun loadImageToAvatarView(photoFile: File) {
        binding.uiRegisterAvatarImageView.load(photoFile) {
            transformations(CircleCropTransformation())
        }
        viewModel.avatarVisible.value = true
    }

    private fun startCamera() {
        val fileName = "${System.currentTimeMillis()}.jpg"
        viewModel.avatarImage.value = fileName

        val photoFile = File(photosDir, fileName)
        photoFile.createNewFile()

        takePhotoLauncher.launch(
            FileProvider.getUriForFile(
                requireContext(),
                "${BuildConfig.APPLICATION_ID}.provider",
                photoFile
            )
        )
    }
}