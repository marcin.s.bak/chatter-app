package pl.mbak.chatterapp.presentation.ui.chat

import android.os.Bundle
import android.view.KeyEvent
import android.view.View
import android.view.inputmethod.EditorInfo
import android.widget.TextView
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.fragment_chat.*
import pl.mbak.chatterapp.R
import pl.mbak.chatterapp.databinding.FragmentChatBinding
import pl.mbak.chatterapp.domain.entity.ChatMessage
import pl.mbak.chatterapp.presentation.base.DataBindingFragment
import pl.mbak.chatterapp.presentation.business.chat.ChatViewModel

@AndroidEntryPoint
class ChatFragment : DataBindingFragment<FragmentChatBinding>(R.layout.fragment_chat) {

    companion object {
        private const val TAG = "ChatFragment"
        fun create(): ChatFragment = ChatFragment()
    }

    private val viewModel: ChatViewModel by viewModels()

    private lateinit var adapter: ChatMessagesAdapter

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        binding.apply {
            message = viewModel.messageText
            sendButtonEnabled = viewModel.sendButtonEnabled
            photoClicked = { requestPhoto() }
            locationClicked = { requestUserLocation() }
            sendClicked = { viewModel.onSend() }
        }
        viewModel.messagesList.observe(viewLifecycleOwner) { updateMessages(it) }

        adapter = ChatMessagesAdapter()
        binding.uiChatMessagesRecyclerView.layoutManager =
            LinearLayoutManager(requireContext(), RecyclerView.VERTICAL, true)
        binding.uiChatMessagesRecyclerView.adapter = adapter
    }

    private fun updateMessages(messages: List<ChatViewModel.MessageVM>) {
        adapter.updateData(messages)
    }

    private fun requestPhoto() {

    }

    private fun requestUserLocation() {

    }
}