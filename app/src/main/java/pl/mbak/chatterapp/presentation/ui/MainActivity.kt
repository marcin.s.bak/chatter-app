package pl.mbak.chatterapp.presentation.ui

import android.app.Activity
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import androidx.core.content.ContextCompat
import dagger.hilt.android.AndroidEntryPoint
import io.reactivex.rxjava3.disposables.Disposable
import pl.mbak.chatterapp.R
import pl.mbak.chatterapp.data.repositories.AuthTokenRepository
import pl.mbak.chatterapp.presentation.ui.chat.ChatActivity
import pl.mbak.chatterapp.presentation.ui.login.LoginFragment
import pl.mbak.chatterapp.presentation.utils.startActivity
import javax.inject.Inject

@AndroidEntryPoint
class MainActivity : AppCompatActivity() {

    companion object {
        private val TAG = "MainActivity"
    }

    @Inject
    lateinit var authTokenRepository: AuthTokenRepository

    private var disposable: Disposable = Disposable.disposed()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        Log.i(TAG, "onCreate finished")

        if (savedInstanceState == null) {
            supportFragmentManager.beginTransaction()
                .add(R.id.uiMainContainer, LoginFragment())
                .commit()
        }
    }

    override fun onStart() {
        super.onStart()
        disposable = authTokenRepository.listenToPreferenceChanges()
            .subscribe { navigateToChat() }
    }

    override fun onStop() {
        super.onStop()
        disposable.dispose()
    }

    private fun navigateToChat() {
        startActivity<ChatActivity>()
        finish()
    }

}