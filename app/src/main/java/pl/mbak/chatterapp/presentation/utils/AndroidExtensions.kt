package pl.mbak.chatterapp.presentation.utils

import android.app.Activity
import android.content.Intent

/**
 *
 * Created by marcinbak on 29/10/2020.
 */
inline fun <reified T: Activity> Activity.startActivity() {
    startActivity(Intent(this, T::class.java))
}