package pl.mbak.chatterapp.presentation.business

import androidx.annotation.VisibleForTesting
import androidx.hilt.Assisted
import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.*
import io.reactivex.rxjava3.disposables.Disposable
import io.reactivex.rxjava3.schedulers.Schedulers
import pl.mbak.chatterapp.BuildConfig
import pl.mbak.chatterapp.R
import pl.mbak.chatterapp.data.api.AuthenticationApi
import pl.mbak.chatterapp.data.api.LoginRequest
import pl.mbak.chatterapp.domain.usercases.LoginUseCase

/**
 *
 * Created by marcinbak on 10/27/20.
 */
class LoginViewModel @ViewModelInject constructor(
    @Assisted private val savedStateHandle: SavedStateHandle,
    private val loginUseCase: LoginUseCase
) : ViewModel() {

    companion object {
        private const val TAG = "LoginViewModel"

        @VisibleForTesting
        internal const val EMAIL_KEY = "EMAIL_KEY"

        @VisibleForTesting
        internal const val PASSWORD_KEY = "PASSWORD_KEY"
    }

    val email: MutableLiveData<String> = savedStateHandle.getLiveData(EMAIL_KEY)
    val password: MutableLiveData<String> = savedStateHandle.getLiveData(PASSWORD_KEY)

    val loginEnabled: LiveData<Boolean> = MediatorLiveData<Boolean>().apply {
        addSource(email) {
            value = enableDisableLoginButton()
        }
        addSource(password) {
            value = enableDisableLoginButton()
        }
    }

    val errorMessage: MutableLiveData<Int?> = MutableLiveData()
    val errorVisible: LiveData<Boolean> = errorMessage.map { it != null }
    val loadingVisible: MutableLiveData<Boolean> = MutableLiveData(false)

    var loginDisposable: Disposable = Disposable.empty()

    fun onLogin() {
        val email = email.value ?: return
        val password = password.value ?: return

        loadingVisible.value = true

        loginDisposable =
            loginUseCase.login(email, password)
                .doOnTerminate { loadingVisible.postValue(false) }
                .subscribe(
                    { errorMessage.postValue(null) },
                    { errorMessage.postValue(R.string.login_error_label) }
                )
    }

    private fun enableDisableLoginButton(): Boolean {
        val email = email.value
        val password = password.value
        return email.isNullOrEmpty().not() && password.isNullOrEmpty().not()
    }

    override fun onCleared() {
        loginDisposable.dispose()
    }

}