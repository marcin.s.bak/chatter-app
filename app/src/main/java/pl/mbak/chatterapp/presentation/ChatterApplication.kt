package pl.mbak.chatterapp.presentation

import android.app.Application
import android.util.Log
import dagger.hilt.android.HiltAndroidApp

/**
 *
 * Created by marcinbak on 10/27/20.
 */
@HiltAndroidApp
class ChatterApplication : Application() {

    companion object {
        private const val TAG = "ChatterApplication"
    }

    override fun onCreate() {
        super.onCreate()
        Log.i(TAG, "Application created")
    }

}