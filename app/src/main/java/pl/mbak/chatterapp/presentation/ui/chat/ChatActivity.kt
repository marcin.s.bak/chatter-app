package pl.mbak.chatterapp.presentation.ui.chat

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import dagger.hilt.android.AndroidEntryPoint
import pl.mbak.chatterapp.R

@AndroidEntryPoint
class ChatActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_chat)


        // check if logged in - if not finish it and start MainActivity

        if (savedInstanceState == null) {
            supportFragmentManager.beginTransaction()
                .add(R.id.uiChatContainer, ChatFragment.create())
                .commit()
        }
    }

}