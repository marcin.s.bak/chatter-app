package pl.mbak.chatterapp.presentation.ui.chat

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import pl.mbak.chatterapp.R
import pl.mbak.chatterapp.databinding.LiChatMessageBinding
import pl.mbak.chatterapp.domain.entity.ChatMessage
import pl.mbak.chatterapp.presentation.business.chat.ChatViewModel
import kotlin.random.Random

/**
 *
 * Created by marcinbak on 29/10/2020.
 */
class ChatMessagesAdapter : RecyclerView.Adapter<ChatMessagesAdapter.MessageViewHolder>() {

    class MessageViewHolder(val binding: LiChatMessageBinding) :
        RecyclerView.ViewHolder(binding.root)

    private var messageList = mutableListOf<ChatViewModel.MessageVM>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MessageViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val binding = DataBindingUtil.inflate<LiChatMessageBinding>(
            inflater,
            R.layout.li_chat_message,
            parent,
            false
        )
        return MessageViewHolder(binding)
    }

    override fun onBindViewHolder(holder: MessageViewHolder, position: Int) {
        val item = messageList[position]
        holder.binding.apply {
            text = item.text
            date = item.date
            author = item.fullName
            startOrEnd = item.startOrEnd
        }
    }

    override fun getItemCount(): Int {
        return messageList.size
    }

    fun updateData(list: List<ChatViewModel.MessageVM>) {
        val oldItemsCount = messageList.size
        messageList.clear()
        messageList.addAll(list)

        notifyItemRangeInserted(0, list.size - oldItemsCount)
//        notifyDataSetChanged()
    }

}