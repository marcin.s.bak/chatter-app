package pl.mbak.chatterapp.presentation.business

import androidx.hilt.Assisted
import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.*
import io.reactivex.rxjava3.disposables.Disposable
import pl.mbak.chatterapp.domain.usercases.RegisterAndLoginUseCase

/**
 *
 * Created by marcinbak on 10/27/20.
 */
class RegisterViewModel @ViewModelInject constructor(
    @Assisted private val savedStateHandle: SavedStateHandle,
    private val registerAndLoginUseCase: RegisterAndLoginUseCase,
) : ViewModel() {

    companion object {
        internal const val EMAIL_KEY = "EMAIL_KEY"
        private const val PASSWORD_KEY = "PASSWORD_KEY"
        private const val REPEAT_PASSWORD_KEY = "REPEAT_PASSWORD_KEY"
        private const val FIRST_NAME_KEY = "FIRST_NAME_KEY"
        private const val LAST_NAME_KEY = "LAST_NAME_KEY"
        private const val TOS_ACCEPTED_KEY = "TOS_ACCEPTED_KEY"
        private const val AVATAR_IMAGE_KEY = "AVATAR_IMAGE_KEY"
        private const val AVATAR_IMAGE_VISIBLE_KEY = "AVATAR_IMAGE_VISIBLE_KEY"
    }

    val email: MutableLiveData<String> = savedStateHandle.getLiveData(EMAIL_KEY)
    val password: MutableLiveData<String> = savedStateHandle.getLiveData(PASSWORD_KEY)
    val repeatPassword: MutableLiveData<String> = savedStateHandle.getLiveData(REPEAT_PASSWORD_KEY)
    val firstName: MutableLiveData<String> = savedStateHandle.getLiveData(FIRST_NAME_KEY)
    val lastName: MutableLiveData<String> = savedStateHandle.getLiveData(LAST_NAME_KEY)
    val tosAccepted: MutableLiveData<Boolean> = savedStateHandle.getLiveData(TOS_ACCEPTED_KEY)

    val registerEnabled: LiveData<Boolean> = MediatorLiveData<Boolean>().apply {
        addSource(email) { value = enableDisableRegisterButton() }
        addSource(password) { value = enableDisableRegisterButton() }
        addSource(repeatPassword) { value = enableDisableRegisterButton() }
        addSource(firstName) { value = enableDisableRegisterButton() }
        addSource(lastName) { value = enableDisableRegisterButton() }
        addSource(tosAccepted) { value = enableDisableRegisterButton() }
    }

    val avatarImage: MutableLiveData<String?> = savedStateHandle.getLiveData(AVATAR_IMAGE_KEY)
    val avatarVisible: MutableLiveData<Boolean> =
        savedStateHandle.getLiveData(AVATAR_IMAGE_VISIBLE_KEY)

    val loadingVisible: MutableLiveData<Boolean> = MutableLiveData(false)

    var registrationDisposable: Disposable = Disposable.empty()

    fun onCreateAccount() {
        val email = email.value ?: return
        val password = password.value ?: return
        val firstName = firstName.value ?: return
        val lastName = lastName.value ?: return

        loadingVisible.value = true

        registrationDisposable =
            registerAndLoginUseCase.register(email, password, firstName, lastName)
                .doOnTerminate { loadingVisible.postValue(false) }
                .subscribe({
                }, {
                })
    }

    private fun enableDisableRegisterButton(): Boolean {
        val email = email.value
        val password = password.value
        val repeatPassword = repeatPassword.value
        val firstName = firstName.value
        val lastName = lastName.value

        val tosAccepted = tosAccepted.value ?: false

        return email.isNullOrEmpty().not() && password.isNullOrEmpty()
            .not() && repeatPassword.isNullOrEmpty().not() && firstName.isNullOrEmpty()
            .not() && lastName.isNullOrEmpty().not() && tosAccepted
    }

    override fun onCleared() {
        registrationDisposable.dispose()
    }

}