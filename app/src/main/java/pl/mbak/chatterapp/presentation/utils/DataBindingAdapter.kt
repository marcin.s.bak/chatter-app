package pl.mbak.chatterapp.presentation.utils

import android.view.Gravity
import android.view.View
import android.widget.FrameLayout
import android.widget.TextView
import androidx.databinding.BindingAdapter
import androidx.lifecycle.LiveData

/**
 *
 * Created by marcinbak on 10/27/20.
 */
@set:BindingAdapter("visibleOrNot")
var View.visibleOrNot: Boolean
    get() = this.visibility == View.VISIBLE
    set(value) {
        visibility = if (value) View.VISIBLE else View.INVISIBLE
    }

@BindingAdapter("textRes")
fun TextView.setTextRes(liveData: LiveData<Int?>) {
    val resId = liveData.value
    if (resId == null) text = null else setText(resId)
}

@BindingAdapter("layoutGravity")
fun View.setLayoutGravity(startOrEnd: Boolean) {
    val lp = (this.layoutParams as? FrameLayout.LayoutParams) ?: return
    lp.gravity = if (startOrEnd) Gravity.START else Gravity.END
}