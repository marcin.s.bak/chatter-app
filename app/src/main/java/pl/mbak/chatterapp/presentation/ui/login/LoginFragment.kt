package pl.mbak.chatterapp.presentation.ui.login

import android.os.Bundle
import android.view.View
import androidx.fragment.app.viewModels
import dagger.hilt.android.AndroidEntryPoint
import io.reactivex.rxjava3.disposables.Disposable
import pl.mbak.chatterapp.R
import pl.mbak.chatterapp.presentation.base.DataBindingFragment
import pl.mbak.chatterapp.presentation.business.LoginViewModel
import pl.mbak.chatterapp.databinding.FragmentLoginBinding

@AndroidEntryPoint
class LoginFragment : DataBindingFragment<FragmentLoginBinding>(R.layout.fragment_login) {

    companion object {
        private val TAG = "LoginFragment"
    }

    private val viewModel: LoginViewModel by viewModels()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        binding.apply {
            email = viewModel.email
            password = viewModel.password
            loginEnabled = viewModel.loginEnabled

            errorMessage = viewModel.errorMessage
            errorVisible = viewModel.errorVisible

            loadingVisible = viewModel.loadingVisible

            loginClicked = { viewModel.onLogin() }
            createAccountClicked = { navigateToRegister() }
        }
    }

    private var disposable: Disposable = Disposable.disposed()

    override fun onStop() {
        super.onStop()
        disposable.dispose()
    }

    private fun navigateToRegister() {
        val fragment = RegisterFragment.create(viewModel.email.value)

        parentFragmentManager.beginTransaction()
            .setCustomAnimations(
                R.anim.slide_in_right,
                R.anim.slide_out_left,
                R.anim.slide_in_left,
                R.anim.slide_out_right,
            )
            .replace(R.id.uiMainContainer, fragment)
            .addToBackStack(null)
            .commit()
    }

}