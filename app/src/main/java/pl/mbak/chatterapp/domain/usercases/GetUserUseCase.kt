package pl.mbak.chatterapp.domain.usercases

import dagger.Reusable
import io.reactivex.rxjava3.core.Single
import pl.mbak.chatterapp.data.api.UserApi
import pl.mbak.chatterapp.data.repositories.AuthTokenRepository
import pl.mbak.chatterapp.data.repositories.UsersRepository
import pl.mbak.chatterapp.domain.entity.ChatUser
import javax.inject.Inject

/**
 *
 * Created by marcinbak on 29/10/2020.
 */
@Reusable
class GetUserUseCase @Inject constructor(
    private val authTokenRepository: AuthTokenRepository,
    private val userApi: UserApi,
    private val usersRepository: UsersRepository
) {

    companion object {
        private const val TAG = "GetUserUseCase"
    }

    fun get(id: String): Single<ChatUser> {
        val token = authTokenRepository.getToken()
            ?: return Single.error(IllegalStateException("No authenticated user!"))

        return usersRepository.getUser(id)
            .switchIfEmpty(
                userApi
                    .getUser(token = token, userId = id)
                    .map { ChatUser(id = id, it.firstName, it.lastName) }
                    .flatMap { usersRepository.saveUser(it) }
            )
    }

}