package pl.mbak.chatterapp.domain.entity

import java.time.LocalDateTime

/**
 *
 * Created by marcinbak on 29/10/2020.
 */
data class ChatMessage(
    val messageId: String,
    val author: ChatMessageAuthor,
    val text: String,
    val timeStamp: LocalDateTime
)

data class ChatMessageAuthor(val id: String, val firstName: String, val lastName: String)

data class ChatUser(val id: String, val firstName: String, val lastName: String)