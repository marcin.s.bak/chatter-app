package pl.mbak.chatterapp.domain.usercases

import dagger.Reusable
import io.reactivex.rxjava3.core.Completable
import pl.mbak.chatterapp.data.api.MessagesApi
import pl.mbak.chatterapp.data.api.PostMessageRequest
import pl.mbak.chatterapp.data.repositories.AuthTokenRepository
import javax.inject.Inject
import kotlin.IllegalStateException

/**
 *
 * Created by marcinbak on 29/10/2020.
 */
@Reusable
class SendMessageUseCase @Inject constructor(
    private val messagesApi: MessagesApi,
    private val authTokenRepository: AuthTokenRepository,
) {

    fun send(message: String): Completable {
        val userId = authTokenRepository.getUserId() ?: return Completable.error(
            IllegalStateException("No authenticated user!")
        )
        val token = authTokenRepository.getToken()
            ?: return Completable.error(IllegalStateException("No authenticated user!"))

        val request = PostMessageRequest(text = message, authorId = userId)
        return messagesApi.postMessage(token, request)
    }

}