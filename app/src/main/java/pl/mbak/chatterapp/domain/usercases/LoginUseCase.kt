package pl.mbak.chatterapp.domain.usercases

import io.reactivex.rxjava3.core.Single
import io.reactivex.rxjava3.schedulers.Schedulers
import pl.mbak.chatterapp.BuildConfig
import pl.mbak.chatterapp.data.api.*
import pl.mbak.chatterapp.data.repositories.AuthTokenRepository
import javax.inject.Inject

/**
 *
 * Created by marcinbak on 28/10/2020.
 */
class LoginUseCase @Inject constructor(
    private val authenticationApi: AuthenticationApi,
    private val authTokenRepository: AuthTokenRepository,
) {

    fun login(email: String, password: String): Single<Boolean> {
        val request = LoginRequest(email, password)

        return authenticationApi.login(BuildConfig.FIREBASE_API_KEY, request)
            .doOnSuccess { saveTokens(it) }
            .subscribeOn(Schedulers.io())
            .map { true }
            .onErrorReturnItem(false)
    }

    private fun saveTokens(response: LoginResponse) {
        authTokenRepository.saveUserId(response.localId)
        authTokenRepository.saveTokens(response.idToken, response.refreshToken)
    }
}