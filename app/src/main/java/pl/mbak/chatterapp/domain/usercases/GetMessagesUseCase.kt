package pl.mbak.chatterapp.domain.usercases

import android.os.Build
import android.util.Log
import androidx.annotation.RequiresApi
import dagger.Reusable
import io.reactivex.rxjava3.core.Flowable
import pl.mbak.chatterapp.data.api.MessageResponse
import pl.mbak.chatterapp.data.api.MessagesApi
import pl.mbak.chatterapp.data.repositories.AuthTokenRepository
import pl.mbak.chatterapp.domain.entity.ChatMessage
import pl.mbak.chatterapp.domain.entity.ChatMessageAuthor
import pl.mbak.chatterapp.domain.entity.ChatUser
import java.time.Instant
import java.time.LocalDateTime
import java.time.ZoneId
import java.util.concurrent.TimeUnit
import javax.inject.Inject

/**
 *
 * Created by marcinbak on 29/10/2020.
 */
@Reusable
class GetMessagesUseCase @Inject constructor(
    private val messagesApi: MessagesApi,
    private val authTokenRepository: AuthTokenRepository,
    private val getUserUseCase: GetUserUseCase,
) {

    companion object {
        private const val TAG = "GetMessagesUseCase"
    }

    fun get(): Flowable<List<ChatMessage>> {
        val token = authTokenRepository.getToken()
            ?: return Flowable.error(IllegalStateException("No authenticated user!"))

        return messagesApi
            .getMessages(token)
            .flatMap { response ->
                Flowable
                    .fromIterable(response.values.map { it.authorId }.distinct())
                    .flatMapSingle { getUserUseCase.get(it) }
                    .toList()
                    .map { mergeUsersWithResponse(response, it) }
            }
            .onErrorReturnItem(emptyList())
            .repeatWhen { completed -> completed.delay(10, TimeUnit.SECONDS) }
    }

    private fun mergeUsersWithResponse(
        response: Map<String, MessageResponse>,
        users: List<ChatUser>
    ): List<ChatMessage> {
        val authors =
            users
                .map { ChatMessageAuthor(it.id, it.firstName, it.lastName) }
                .associateBy { it.id }

        return response.map {
            val author = authors[it.value.authorId] ?: ChatMessageAuthor("NONE", "Unknown", "user")
            ChatMessage(it.key, author, it.value.text, it.value.timeStamp.mapTimestampToDateTime())
        }
    }

    private fun Long.mapTimestampToDateTime(): LocalDateTime {
        return Instant.ofEpochMilli(this)
            .atZone(ZoneId.systemDefault()).toLocalDateTime()
    }

}