package pl.mbak.chatterapp.domain.usercases

import dagger.Reusable
import io.reactivex.rxjava3.core.Single
import io.reactivex.rxjava3.schedulers.Schedulers
import pl.mbak.chatterapp.BuildConfig
import pl.mbak.chatterapp.data.api.*
import pl.mbak.chatterapp.data.repositories.AuthTokenRepository
import javax.inject.Inject

/**
 *
 * Created by marcinbak on 28/10/2020.
 */
@Reusable
class RegisterAndLoginUseCase @Inject constructor(
    private val authenticationApi: AuthenticationApi,
    private val userApi: UserApi,
    private val authTokenRepository: AuthTokenRepository,
) {

    fun register(
        email: String,
        password: String,
        firstName: String,
        lastName: String,
    ): Single<Boolean> {
        val request = RegisterRequest(email, password)

        return authenticationApi.register(BuildConfig.FIREBASE_API_KEY, request)
            .doOnSuccess { saveTokens(it) }
            .flatMap { saveUserData(it, firstName, lastName) }
            .subscribeOn(Schedulers.io())
            .map { true }
            .onErrorReturnItem(false)
    }

    private fun saveUserData(
        registerResponse: RegisterResponse,
        firstName: String,
        lastName: String
    ): Single<Unit> {
        return userApi.saveUser(
            registerResponse.idToken,
            mapOf(registerResponse.localId to SaveUserData(firstName, lastName))
        )
    }

    private fun saveTokens(response: RegisterResponse) {
        authTokenRepository.saveUserId(response.localId)
        authTokenRepository.saveTokens(response.idToken, response.refreshToken)
    }

}