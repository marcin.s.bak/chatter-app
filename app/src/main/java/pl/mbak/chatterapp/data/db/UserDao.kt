package pl.mbak.chatterapp.data.db

import androidx.room.*
import io.reactivex.rxjava3.core.Completable
import io.reactivex.rxjava3.core.Maybe

/**
 *
 * Created by marcinbak on 30/10/2020.
 */
@Dao
interface UserDao {

    companion object {
        const val USER_TABLE = "user"

        const val COL_ID = "id"
        const val COL_FIRST_NAME = "first_name"
        const val COL_LAST_NAME = "last_name"
    }

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertUser(user: UserEntity): Completable

    @Query("SELECT * FROM $USER_TABLE WHERE $COL_ID=:userId")
    fun getUser(userId: String): Maybe<UserEntity>

    @Entity(
        tableName = USER_TABLE
    )
    data class UserEntity(
        @PrimaryKey @ColumnInfo(name = COL_ID) val id: String,
        @ColumnInfo(name = COL_FIRST_NAME) val firstName: String,
        @ColumnInfo(name = COL_LAST_NAME) val lastName: String,
    )

}