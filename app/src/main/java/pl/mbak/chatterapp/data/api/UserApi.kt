package pl.mbak.chatterapp.data.api

import com.squareup.moshi.JsonClass
import io.reactivex.rxjava3.core.Single
import retrofit2.http.*

/**
 *
 * Created by marcinbak on 28/10/2020.
 */
interface UserApi {

    @PATCH("users.json")
    fun saveUser(
        @Query("auth") token: String,
        @Body request: Map<String, SaveUserData>
    ): Single<Unit>


    @GET("users/{userId}.json")
    fun getUser(
        @Path("userId") userId: String,
        @Query("auth") token: String
    ): Single<UserDataResponse>

}

@JsonClass(generateAdapter = true)
data class SaveUserData(
    val firstName: String,
    val lastName: String,
)

@JsonClass(generateAdapter = true)
data class UserDataResponse(
    val firstName: String,
    val lastName: String,
)