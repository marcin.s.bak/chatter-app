package pl.mbak.chatterapp.data.api

import com.squareup.moshi.JsonClass
import io.reactivex.rxjava3.core.Single
import retrofit2.http.Body
import retrofit2.http.Headers
import retrofit2.http.POST
import retrofit2.http.Query

/**
 *
 * Created by marcinbak on 10/27/20.
 */
interface AuthenticationApi {

    @POST("v1/accounts:signUp")
    @Headers("Content-Type: application/json")
    fun register(
        @Query("key") apiKey: String,
        @Body request: RegisterRequest
    ): Single<RegisterResponse>

    @POST("v1/accounts:signInWithPassword")
    @Headers("Content-Type: application/json;")
    fun login(@Query("key") apiKey: String, @Body request: LoginRequest): Single<LoginResponse>

}

@JsonClass(generateAdapter = true)
data class RegisterRequest(
    val email: String,
    val password: String,
    val returnSecureToken: Boolean = true,
)

@JsonClass(generateAdapter = true)
data class RegisterResponse(
    val idToken: String,
    val email: String,
    val refreshToken: String,
    val expiresIn: String,
    val localId: String,
)

@JsonClass(generateAdapter = true)
data class LoginRequest(
    val email: String,
    val password: String,
    val returnSecureToken: Boolean = true,
)

@JsonClass(generateAdapter = true)
data class LoginResponse(
    val idToken: String,
    val email: String,
    val refreshToken: String,
    val expiresIn: String,
    val localId: String,
    val registered: Boolean,
)