package pl.mbak.chatterapp.data.di

import android.content.Context
import android.content.SharedPreferences
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ApplicationComponent
import dagger.hilt.android.qualifiers.ApplicationContext

/**
 *
 * Created by marcinbak on 28/10/2020.
 */
@InstallIn(ApplicationComponent::class)
@Module
class AndroidModule {

    companion object {
        private const val SHARED_PREFS = "SHARED_PREFS"
    }

    @Provides
    fun provideSharedPreferences(@ApplicationContext context: Context): SharedPreferences {
        return context.getSharedPreferences(SHARED_PREFS, Context.MODE_PRIVATE)
    }
}