package pl.mbak.chatterapp.data.di

import android.content.Context
import androidx.room.Room
import androidx.room.migration.Migration
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ApplicationComponent
import dagger.hilt.android.qualifiers.ApplicationContext
import pl.mbak.chatterapp.BuildConfig
import pl.mbak.chatterapp.data.db.ChatterAppDatabase
import pl.mbak.chatterapp.data.db.UserDao
import javax.inject.Singleton

/**
 *
 * Created by marcinbak on 30/10/2020.
 */
@InstallIn(ApplicationComponent::class)
@Module
class DatabaseModule {

    companion object {
        private const val DB_NAME = "chatterapp_db"
    }

    @Singleton
    @Provides
    fun provideDatabase(@ApplicationContext context: Context): ChatterAppDatabase {
        return Room
            .databaseBuilder(
                context, ChatterAppDatabase::class.java, DB_NAME
            )
            .apply {
                if (BuildConfig.DEBUG) fallbackToDestructiveMigration()
            }
            .build()
    }

    @Provides
    fun provideUserDao(db: ChatterAppDatabase): UserDao {
        return db.userDao()
    }

}