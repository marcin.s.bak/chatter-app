package pl.mbak.chatterapp.data.api

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass
import io.reactivex.rxjava3.core.Completable
import io.reactivex.rxjava3.core.Single
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST
import retrofit2.http.Query

/**
 *
 * Created by marcinbak on 29/10/2020.
 */
interface MessagesApi {

    @POST("messages.json")
    fun postMessage(@Query("auth") token: String, @Body request: PostMessageRequest): Completable

    @GET("messages.json")
    fun getMessages(@Query("auth") token: String): Single<Map<String, MessageResponse>>

}

@JsonClass(generateAdapter = true)
data class PostMessageRequest(
    val text: String,
    val authorId: String,
    @Json(name = "server_timestamp") val timestamp: Map<String, String> = mapOf(".sv" to "timestamp"),
)

@JsonClass(generateAdapter = true)
data class MessageResponse(
    val text: String,
    val authorId: String,
    @Json(name = "server_timestamp") val timeStamp: Long,
)