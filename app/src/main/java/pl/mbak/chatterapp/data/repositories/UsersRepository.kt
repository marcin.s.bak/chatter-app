package pl.mbak.chatterapp.data.repositories

import dagger.Reusable
import io.reactivex.rxjava3.core.Maybe
import io.reactivex.rxjava3.core.Single
import io.reactivex.rxjava3.schedulers.Schedulers
import pl.mbak.chatterapp.data.db.UserDao
import pl.mbak.chatterapp.domain.entity.ChatUser
import javax.inject.Inject

/**
 *
 * Created by marcinbak on 30/10/2020.
 */
@Reusable
class UsersRepository @Inject constructor(
    private val userDao: UserDao,
) {

    fun getUser(id: String): Maybe<ChatUser> {
        return userDao.getUser(id)
            .map { ChatUser(it.id, it.firstName, it.lastName) }
            .subscribeOn(Schedulers.io())
    }

    fun saveUser(user: ChatUser): Single<ChatUser> {
        return userDao
            .insertUser(UserDao.UserEntity(user.id, user.firstName, user.lastName))
            .toSingleDefault(user)
            .subscribeOn(Schedulers.io())
    }

}