package pl.mbak.chatterapp.data.repositories

import android.content.SharedPreferences
import androidx.core.content.edit
import dagger.Reusable
import io.reactivex.rxjava3.core.BackpressureStrategy
import io.reactivex.rxjava3.core.Flowable
import io.reactivex.rxjava3.disposables.Disposable
import javax.inject.Inject
import javax.inject.Singleton

/**
 *
 * Created by marcinbak on 28/10/2020.
 */
@Reusable
class AuthTokenRepository @Inject constructor(
    private val sharedPreferences: SharedPreferences,
) {

    companion object {
        private const val TOKEN_KEY = "TOKEN_KEY"
        private const val REFRESH_TOKEN_KEY = "REFRESH_TOKEN_KEY"
        private const val USER_ID_KEY = "USER_ID_KEY"
    }

    fun saveTokens(token: String, refreshToken: String) {
        sharedPreferences.edit {
            putString(TOKEN_KEY, token)
            putString(REFRESH_TOKEN_KEY, refreshToken)
            commit()
        }
    }

    fun saveUserId(userId: String) {
        sharedPreferences.edit {
            putString(USER_ID_KEY, userId)
            commit()
        }
    }

    fun getUserId(): String? {
        return sharedPreferences.getString(USER_ID_KEY, null)
    }

    fun getToken(): String? {
        return sharedPreferences.getString(TOKEN_KEY, null)
    }

    fun getRefreshToken(): String? {
        return sharedPreferences.getString(REFRESH_TOKEN_KEY, null)
    }

    fun listenToPreferenceChanges(): Flowable<String> {
        return Flowable.create({ emitter ->
            sharedPreferences.getString(TOKEN_KEY, null)?.let { emitter.onNext(it) }

            val listener = SharedPreferences.OnSharedPreferenceChangeListener { prefs, key ->
                // inform MainActivity about token change
                if (key == TOKEN_KEY) {
                    prefs.getString(TOKEN_KEY, null)?.let { emitter.onNext(it) }
                }
            }
            sharedPreferences.registerOnSharedPreferenceChangeListener(listener)

            emitter.setDisposable(Disposable.fromAction {
                sharedPreferences.unregisterOnSharedPreferenceChangeListener(listener)
            })
        }, BackpressureStrategy.LATEST)
    }

}