package pl.mbak.chatterapp.data.db

import androidx.room.Database
import androidx.room.RoomDatabase

/**
 *
 * Created by marcinbak on 30/10/2020.
 */
@Database(
    entities = [
        UserDao.UserEntity::class
    ],
    version = 1,
)
abstract class ChatterAppDatabase : RoomDatabase() {
    abstract fun userDao(): UserDao
}