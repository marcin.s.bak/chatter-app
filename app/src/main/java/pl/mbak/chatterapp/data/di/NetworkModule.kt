package pl.mbak.chatterapp.data.di

import dagger.Module
import dagger.Provides
import dagger.Reusable
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ApplicationComponent
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import pl.mbak.chatterapp.data.api.AuthenticationApi
import pl.mbak.chatterapp.data.api.MessagesApi
import pl.mbak.chatterapp.data.api.UserApi
import retrofit2.Retrofit
import retrofit2.adapter.rxjava3.RxJava3CallAdapterFactory
import retrofit2.converter.moshi.MoshiConverterFactory

/**
 *
 * Created by marcinbak on 28/10/2020.
 */
@InstallIn(ApplicationComponent::class)
@Module
class NetworkModule {

    @Provides
    @Reusable
    fun provideOkHttpClient(): OkHttpClient {
        val loggingInterceptor = HttpLoggingInterceptor()
        loggingInterceptor.level = HttpLoggingInterceptor.Level.NONE
        return OkHttpClient.Builder()
            .addInterceptor(loggingInterceptor)
            .build()
    }

    @Provides
    @Reusable
    fun provideRetrofit(client: OkHttpClient): Retrofit.Builder {
        return Retrofit.Builder()
            .addConverterFactory(MoshiConverterFactory.create())
            .addCallAdapterFactory(RxJava3CallAdapterFactory.create())
            .client(client)
    }

    @Provides
    @Reusable
    fun provideAuthenticationApi(retrofitBuilder: Retrofit.Builder): AuthenticationApi {
        val retrofit = retrofitBuilder
            .baseUrl("https://identitytoolkit.googleapis.com/")
            .build()

        return retrofit.create(AuthenticationApi::class.java)
    }

    @Provides
    @Reusable
    fun provideUserApi(retrofitBuilder: Retrofit.Builder): UserApi {
        val retrofit = retrofitBuilder
            .baseUrl("https://chatter-app-27717.firebaseio.com/")
            .build()

        return retrofit.create(UserApi::class.java)
    }

    @Provides
    @Reusable
    fun provideMessagesApi(retrofitBuilder: Retrofit.Builder): MessagesApi {
        val retrofit = retrofitBuilder
            .baseUrl("https://chatter-app-27717.firebaseio.com/")
            .build()

        return retrofit.create(MessagesApi::class.java)
    }

}
