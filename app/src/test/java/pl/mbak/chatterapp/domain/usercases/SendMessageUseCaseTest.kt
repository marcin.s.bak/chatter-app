package pl.mbak.chatterapp.domain.usercases

import io.mockk.MockKAnnotations
import io.mockk.every
import io.mockk.impl.annotations.MockK
import io.mockk.slot
import io.mockk.verify
import io.reactivex.rxjava3.core.Completable
import org.junit.AfterClass
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.BeforeClass
import org.junit.Test
import pl.mbak.chatterapp.data.api.MessagesApi
import pl.mbak.chatterapp.data.api.PostMessageRequest
import pl.mbak.chatterapp.data.repositories.AuthTokenRepository
import java.lang.IllegalStateException
import java.net.SocketTimeoutException

/**
 * Created by marcinbak on 30/10/2020.
 */
class SendMessageUseCaseTest {

    companion object {

        @BeforeClass
        @JvmStatic
        fun setUpClass() {
        }

        @AfterClass
        @JvmStatic
        fun tearDownClass() {
        }
    }

    private lateinit var sut: SendMessageUseCase

    @MockK
    lateinit var messagesApi: MessagesApi

    @MockK
    lateinit var authTokenRepository: AuthTokenRepository

    @Before
    fun setUp() {
        MockKAnnotations.init(this)

        sut = SendMessageUseCase(messagesApi, authTokenRepository)
    }

    @Test
    fun `send completed successfully`() {
        // Having
        val message = "message"
        val userId = "userId"
        val token = "token"

        val requestSlot = slot<PostMessageRequest>()

        every { authTokenRepository.getUserId() } returns userId
        every { authTokenRepository.getToken() } returns token
        every {
            messagesApi.postMessage(
                any(),
                capture(requestSlot)
            )
        } returns Completable.complete()

        // When
        val result = sut.send(message).test()

        // Then
        verify { messagesApi.postMessage(token, any()) }

        assertEquals(userId, requestSlot.captured.authorId)
        assertEquals(message, requestSlot.captured.text)
        assertEquals(mapOf(".sv" to "timestamp"), requestSlot.captured.timestamp)

        result.assertComplete()
        result.assertNoErrors()
    }

    @Test
    fun `send completed with error`() {
        // Having
        val message = "message"
        val userId = "userId"
        val token = "token"

        every { authTokenRepository.getUserId() } returns userId
        every { authTokenRepository.getToken() } returns token

        every { messagesApi.postMessage(any(), any()) } returns Completable.error(
            SocketTimeoutException()
        )

        // When
        val result = sut.send(message).test()

        // Then
        result.assertError {
            it is SocketTimeoutException && it.message == null
        }
    }

    @Test
    fun `send with exception when no userId`() {
        // Having
        val message = "message"
        val userId = null
        val token = "token"

        every { authTokenRepository.getUserId() } returns userId
        every { authTokenRepository.getToken() } returns token

        every { messagesApi.postMessage(any(), any()) } returns Completable.complete()

        // When
        val result = sut.send(message).test()

        // Then
        verify(exactly = 0) { messagesApi.postMessage(any(), any()) }
        result.assertError(IllegalStateException::class.java)
    }

    @Test
    fun `send with exception when no token`() {
        // Having
        val message = "message"
        val userId = "userId"
        val token = null

        every { authTokenRepository.getUserId() } returns userId
        every { authTokenRepository.getToken() } returns token

        every { messagesApi.postMessage(any(), any()) } returns Completable.complete()

        // When
        val result = sut.send(message).test()

        // Then
        result.assertError(IllegalStateException::class.java)
    }
}