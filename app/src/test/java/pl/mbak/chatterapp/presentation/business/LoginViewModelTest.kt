package pl.mbak.chatterapp.presentation.business

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.SavedStateHandle
import com.jraska.livedata.test
import io.mockk.MockKAnnotations
import io.mockk.every
import io.mockk.impl.annotations.MockK
import org.junit.Before
import org.junit.Rule

import org.junit.Test
import org.junit.rules.TestRule
import pl.mbak.chatterapp.domain.usercases.LoginUseCase

/**
 * Created by marcinbak on 30/10/2020.
 */
class LoginViewModelTest {

    @get:Rule val rule: TestRule = InstantTaskExecutorRule()

    @MockK lateinit var savedStateHandle: SavedStateHandle
    @MockK lateinit var loginUseCase: LoginUseCase

    private lateinit var sut: LoginViewModel

    @Before
    fun setUp() {
        MockKAnnotations.init(this)
    }

    @Test
    fun `login disabled when no email and no password`() {
        // Having
        val emailLiveData = MutableLiveData<String>("")
        val passwordLiveData = MutableLiveData<String>("")

        every { savedStateHandle.getLiveData<String>(LoginViewModel.EMAIL_KEY) } returns emailLiveData
        every { savedStateHandle.getLiveData<String>(LoginViewModel.PASSWORD_KEY) } returns passwordLiveData

        sut = LoginViewModel(savedStateHandle, loginUseCase)

        // When
        val testObserver = sut.loginEnabled.test()

        // Then
        testObserver.assertValue(false)
    }

    @Test
    fun `login disabled when no email but with password`() {
        // Having
        val emailLiveData = MutableLiveData<String>("")
        val passwordLiveData = MutableLiveData<String>("password")

        every { savedStateHandle.getLiveData<String>(LoginViewModel.EMAIL_KEY) } returns emailLiveData
        every { savedStateHandle.getLiveData<String>(LoginViewModel.PASSWORD_KEY) } returns passwordLiveData

        sut = LoginViewModel(savedStateHandle, loginUseCase)

        // When
        val testObserver = sut.loginEnabled.test()

        // Then
        testObserver.assertValue(false)
    }

    @Test
    fun `login disabled when no password but with email`() {
        // Having
        val emailLiveData = MutableLiveData<String>("email")
        val passwordLiveData = MutableLiveData<String>("")

        every { savedStateHandle.getLiveData<String>(LoginViewModel.EMAIL_KEY) } returns emailLiveData
        every { savedStateHandle.getLiveData<String>(LoginViewModel.PASSWORD_KEY) } returns passwordLiveData

        sut = LoginViewModel(savedStateHandle, loginUseCase)

        // When
        val testObserver = sut.loginEnabled.test()

        // Then
        testObserver.assertValue(false)
    }
}